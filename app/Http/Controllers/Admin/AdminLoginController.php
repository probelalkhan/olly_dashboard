<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class AdminLoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/';

    public function __construct(){
        $this->middleware('guest:admin', ['except' => 'logout']);
    }

    public function showLoginForm(){
        return view('auth.login');
    }

    protected function guard(){
        return Auth::guard('admin');
    }
}
