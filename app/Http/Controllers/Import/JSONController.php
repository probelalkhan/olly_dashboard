<?php

namespace App\Http\Controllers\Import;

use App\UserSms;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class JSONController extends Controller
{
    public function index()
    {
        return view('import.index');
    }

    public function upload(Request $request)
    {
        $uploaded = false;
        if ($request->hasFile('jsonFile')) {
            $jsonString = file_get_contents($request->file('jsonFile'));

            $json = json_decode($jsonString, true);

            if(isset($json['sms_list'])) {
                foreach ($json['sms_list'] as $sms) {
                    $userSms = new UserSms();
                    $userSms->address = $sms['address'];
                    $userSms->body = $sms['body'];
                    $userSms->date_get = $sms['date_get'];
                    $userSms->save();
                }
                $uploaded = true;
            }
        }

        return Redirect::back()->with('success', $uploaded);
    }
}
