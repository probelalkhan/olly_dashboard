@extends('adminlte::page')

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Import JSONs</h3>
        </div>

        <div class="box-body">
            <div class="row">
                <div class="col-md-2">
                    <form action="{{ Request::url() }}" enctype="multipart/form-data" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="jsonFileInput">JSON Import</label>
                            <input type="file" id="jsonFileInput" name="jsonFile">
                            <p class="help-block">Choose the JSON File you want to Upload</p>
                        </div>

                        <button type="submit" class="btn btn-primary">Import</button>
                    </form>

                    <br/>

                    @if(Session::has('success'))
                        @if(Session('success'))
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <strong>Great!</strong> JSON Imported.
                            </div>
                        @else
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <strong>Oops!</strong> Something Wrong.
                            </div>
                        @endif
                    @endif
                </div>
            </div>

        </div>
    </div>
@endsection
